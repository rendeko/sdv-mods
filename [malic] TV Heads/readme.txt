TV Heads
by Malic for Stardew Valley

Adds a collection of TV-head cosplay pieces.
	
REQUIREMENTS
	SMAPI
	ContentPatcher
	JsonAssets
	
INSTALLATION
	Copy the "[malic] TV Heads" folder into your Stardew Valley/Mods directory.
	
CHANGELOG
	v1.0.0 - release

LICENSING
	Spritework is under CC BY-SA 4.0 (credit and don't change the license)

CREDITS
	malic