|v2.3.0| Portraits |  Spritesheet |
|---|---|---|---|---|
|Default| ![](img/portraits/Default-Academia.png) ![](img/portraits/Default-Flannel.png) ![](img/portraits/Default-PinkHoodie.png) ![](img/portraits/Default-Tanktop.png) ![](img/portraits/Default-WhiteHoodie.png) ![](img/portraits/Default-BlackBra.png)  |  ![](img/spritesheet/Default-Academia.png)  ![](img/spritesheet/Default-Flannel.png) ![](img/spritesheet/Default-PinkHoodie.png) ![](img/spritesheet/Default-Tanktop.png) ![](img/spritesheet/Default-WhiteHoodie.png) ![](img/spritesheet/Default-BlackSwimsuit.png) |
|Airyn| ![](img/portraits/Airyn-Academia.png) ![](img/portraits/Airyn-Flannel.png) ![](img/portraits/Airyn-PinkHoodie.png) ![](img/portraits/Airyn-Tanktop.png) ![](img/portraits/Airyn-WhiteHoodie.png) ![](img/portraits/Airyn-BlackBra.png)  |  ![](img/spritesheet/Airyn-Academia.png)  ![](img/spritesheet/Airyn-Flannel.png) ![](img/spritesheet/Airyn-PinkHoodie.png) ![](img/spritesheet/Airyn-Tanktop.png) ![](img/spritesheet/Airyn-WhiteHoodie.png)|
|CodeNameRed|  ![](img/portraits/CodeNameRed.png) |  ![](img/spritesheet/CodeNameRed.png) |
|EssGee|  ![](img/portraits/EssGee.png) | |
|Original| ![](img/portraits/Original.png)  | |
|xdx4900|   | ![](img/spritesheet/xdx4900.png)  |
