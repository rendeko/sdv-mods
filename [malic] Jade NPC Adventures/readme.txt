Jade NPC Adventures Add-on
by Malic for Stardew Valley 1.5

Adds Jade as recruitable for NPC Adventures

TRANSLATIONS
	English
	Korean
	Russian
	Simplified Chinese

REQUIREMENTS
	SMAPI
	ContentPatcher
	NPC Adventures (0.13.0)
	
INSTALLATION
	Copy the "[malic] Jade NPC Adventures Add-on" folder into your Stardew Valley/Mods directory.
	
CHANGELOG
	v1.1.2 - Adding Simplified Chinese translation, changing update key to use subkeys
	v1.1.1 - Adding dummy update key to suppress warning
	v1.1.0 - Adding Korean/Russian translations to main mod (NPC Adventures added support) using NPC Adventures 0.13.0b's new locale feature
	v1.0.0 - release

CREDITS
	malic - dialogue
	yunuangel - Korean translation
	Ghost3lboom - Russian translation
	KatherineC4  - Simplified Chinese translation
