This modders resource adds one non-replacing demo hairstyle through Content Patcher.

I felt too many mods replace hairstyles or append new hairstyles to the end of the hairstyles image, in a way that having multiple hair packs would conflict.
This mod uses a SDV 1.5 feature that allows writing into the Data/HairData asset to define custom hairstyles.
No hairstyles.png overwriting necessary. No Get Glam necessary.

This mod is intended to be used as a modders resource for other mod-makers.

## How it works
We insert a hairstyle spritesheet into the Characters/Farmer/ asset folder with CP.
It can be named anything as long as its consistent within our content.json.

Then we insert lines of data into the Data/HairData asset. Each line defines a new hairstyle.

Data/HairData uses the following format:
"100": "hairstyles2/0/0/true/-1/false",

The table name (100) is a unique ID. We need ours to be unique.
This ID cannot be larger than 2,147,483,647 (the maximum value of Int32)
A common trend with NPC events is to start with the last 4 digits of your NexusMods profile ID, so that is what I've done in this demo.

Then we have the following entries, separated by slashes:
0 - The filename for the file we added to Characters/Farmer/, without filetype.
1 - The tile position for our hairstyle sprite, x coordinate.
2 - The tile position for our hairstyle sprite, y coordinate.
3 - If we have a unique left sprite or not. If false, it uses the right hair flipped. If false, we need a fourth sprite.
4 - Either a unique ID of a separate hairstyle for when player is wearing a hat, or -1 to hide hair.
5 - "is bald style". Currently, just set false.
6 - unused
7 - unused
8 - unused

## Notes
We're likely still limited to ~336 hairstyles with this method, but this is untested. Spacecore may solve this if it occurs.

## Credits
Art by Charles Gabriel. Slightly modified. Under CC-BY 3.0 - https://opengameart.org/content/16x18-rpg-characters-hair-clothing-pack

## Changelog
v1.0.0 - Initial release
