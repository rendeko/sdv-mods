DDLC Natsuki
by Malic for Stardew Valley

A simple retexture of Penny to Natsuki from Doki Doki Literature Club

No portraits at the moment
	
REQUIREMENTS
	SMAPI
	ContentPatcher
	
INSTALLATION
	Copy the "[malic] DDLC Natsuki" folder into your Stardew Valley/Mods directory.
	
CHANGELOG
	v1.0.3 - adding head bob on walking downwards sprites
	v1.0.2 - adding nexus update check
	v1.0.1 - fixing floating pixels in walk cycle
	v1.0.0 - release

CREDITS
	malic
	stardew valley for original sprites