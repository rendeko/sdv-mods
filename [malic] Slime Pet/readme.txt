Slime Pets v1.0.0
by Malic for Stardew Valley

Adds optional slime pet skins for your cats

REQUIREMENTS
	SMAPI
	Adopt'n'Skin by Gathouria

INSTALLATION
	Drop the contents of this folder into your Adopt'n'Skin folder.
	If you're asked to overwrite, rename the new skins with a different number.
	
TO-DO
	Replace idle animation (they gyrating, baby!!!)
	Add special and heart antennas
	
CHANGELOG
	v1.0.0 - release
	
CREDITS
	Sprites from Stardew Valley
	Sprite edits by Malic