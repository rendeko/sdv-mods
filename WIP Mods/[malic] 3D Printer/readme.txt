3D Printer v1.0.0
by Malic for Stardew Valley

Adds a 3D Printer, filament, and codes you can use to print unique items.
Printed items can be recycled back into plastic, adding an interesting new mechanic to the game.

REQUIREMENTS
	SMAPI
	JSON Assets
	ProducerFrameworkMod

INSTALLATION
	Drag the main folder into your Stardew Valley/Mods folder
	
TO-DO
	better sprites lol
	
CHANGELOG
	v1.0.0 - release
	
CREDITS
	Sprites by Malic