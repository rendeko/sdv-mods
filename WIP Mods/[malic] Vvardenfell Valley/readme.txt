Vvardenfell Valley v0.1.0
by Malic for Stardew Valley

Adds the crops of Morrowind to the game, sold by M'aiq in the secret woods
	
REQUIREMENTS
	SMAPI
	ContentPatcher
	JsonAssets
	ShopTileFramework
	TMXLoader
	
INSTALLATION
	Copy the "[malic] Vvardenfell Valley v0.1.0" folder into your Stardew Valley/Mods directory.
	
CHANGELOG
	v0.1.0 - prerelease

CREDITS
	malic
	stardew valley for original sprites
	